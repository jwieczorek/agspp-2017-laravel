<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    @if($errors->has('user_id'))
        <p>
            <strong>{{ $errors->first('user_id') }}</strong>
        </p>
    @endif

    @if(session('message'))
        <p>
            <strong>{{ session('message') }}</strong>
        </p>
    @endif
    <form method="POST">
        {{ csrf_field() }}
        <p>
            <label for="user_id">Username, Email, or Phone Number</label>
            <input type="text" id="user_login" name="user_login"/>
        </p>

        <p>
            <input type="submit" name="send-sms" value="Text me my login code!">
            <input type="submit" name="send-email" value="Email me my login code!">
        </p>

    </form>
</body>
</html>