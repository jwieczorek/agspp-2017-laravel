
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

{{ session('message') }}
<form method="POST">
    {{ csrf_field() }}
    <p>
        <label>Your 6 digit pin number</label>
    </p>
        @include('forms.pin')
    <p>
        <input type="submit" value="Login">

        <label for="remember_me">
            <input type="checkbox" name="remember_me" id="remember_me">: Remember Me
        </label>
    </p>
</form>