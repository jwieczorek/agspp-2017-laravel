<form method="POST">
    {{ csrf_field() }}

    <p>
        <label for="fname">First Name</label>
        <input type="text" id="fname" name="fname" value="{{ old('fname') }}">
    </p>
    @if($errors->has('fname'))
    <p>
        {{ $errors->first('fname') }}
    </p>
    @endif


    <p>
        <label for="lname">Last Name</label>
        <input type="text" id="lname" name="lname" value="{{ old('lname') }}">
    </p>
    @if($errors->has('lname'))
        <p>
            {{ $errors->first('lname') }}
        </p>
    @endif

    <p>
        <label for="username">Username</label>
        <input type="text" id="username" name="username" value="{{ old('username') }}">
    </p>
    @if($errors->has('username'))
        <p>
            {{ $errors->first('username') }}
        </p>
    @endif

    <p>
        <label for="email">Email</label>
        <input type="text" id="email" name="email" value="{{ old('email') }}">
    </p>
    @if($errors->has('email'))
        <p>
            {{ $errors->first('email') }}
        </p>
    @endif

    <p>
        <label for="phone">Phone</label>
        <input type="text" id="phone" name="phone" value="{{ old('phone') }}">
    </p>
    @if($errors->has('phone'))
        <p>
            {{ $errors->first('phone') }}
        </p>
    @endif

    <p>
        <input type="submit" id="register" name="register-activate-sms" value="Activate via Text Message">
        <input type="submit" id="register" name="register-activate-emial" value="Activate via Email">
    </p>



</form>