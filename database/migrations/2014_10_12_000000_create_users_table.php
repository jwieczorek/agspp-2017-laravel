<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name', 100);
            $table->string('last_name', 100);
            $table->string('username', 255)->unique();
            $table->string('email', 255)->unique();
            $table->string('phone', 25)->nullable();
            $table->text('cookie')->nullable();
            $table->boolean('active')->default(0);
            $table->integer('attempts')->default(0);
            $table->integer('locked')->default(0);
            $table->boolean('api_access')->default(0);
            $table->text('remember_token')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
