<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('author');
            $table->string('title');
            $table->string('slug')->nullable();
            $table->string('guide');
            $table->longText('content');
            $table->longText('excerpt')->nullable();
            $table->string('featured_image')->nullable();
            $table->integer('access_role')->nullable();
            $table->string('status')->nullable();
            $table->longText('price')->nullable();
            $table->boolean('bulk_pricing')->default(0)->nullable();
            $table->integer('parent')->nullable();
            $table->string('ping_status')->nullable();
            $table->string('password')->nullable();
            $table->string('type')->nullable();
            $table->string('comments')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('content');
    }
}
