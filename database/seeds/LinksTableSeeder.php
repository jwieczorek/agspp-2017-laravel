<?php

use Illuminate\Database\Seeder;

class LinksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('links')->insert([
            [
                'content_id' => 1,
                'title' => 'Facebook',
                'link' => 'http://google.com'
            ],
            [
                'content_id' => 1,
                'title' => 'Twitter',
                'link' => 'http://google.com'
            ],
            [
                'content_id' => 1,
                'title' => 'LinkedIn',
                'link' => 'http://google.com'
            ],
            [
                'content_id' => 1,
                'title' => 'Google+',
                'link' => 'http://google.com'
            ]
        ]);
    }
}
