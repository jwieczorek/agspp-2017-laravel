<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'first_name' => 'Joshua',
            'last_name' => 'Wieczorek',
            'username' => 'jd7777',
            'email' => 'joshuawieczorek@outlook.com',
            'phone' => '19543288859',
            'api_access' => 1
        ]);
    }
}
