<?php

use Illuminate\Database\Seeder;

class UserPermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_permissions')->insert([
            ['permission_name' => 'content_block_create'],
            ['permission_name' => 'content_block_read'],
            ['permission_name' => 'content_block_update'],
            ['permission_name' => 'content_block_delete'],
            ['permission_name' => 'page_create'],
            ['permission_name' => 'page_read'],
            ['permission_name' => 'page_update'],
            ['permission_name' => 'page_delete'],
            ['permission_name' => 'post_create'],
            ['permission_name' => 'post_read'],
            ['permission_name' => 'post_update'],
            ['permission_name' => 'post_delete'],
            ['permission_name' => 'product_create'],
            ['permission_name' => 'product_read'],
            ['permission_name' => 'product_update'],
            ['permission_name' => 'product_delete'],
            ['permission_name' => 'shops_create'],
            ['permission_name' => 'shops_read'],
            ['permission_name' => 'shops_update'],
            ['permission_name' => 'shops_delete'],
            ['permission_name' => 'shops_config_create'],
            ['permission_name' => 'shops_config_read'],
            ['permission_name' => 'shops_config_update'],
            ['permission_name' => 'shops_config_delete'],
            ['permission_name' => 'spot_price_create'],
            ['permission_name' => 'spot_price_read'],
            ['permission_name' => 'spot_price_update'],
            ['permission_name' => 'spot_price_delete'],
            ['permission_name' => 'store_create'],
            ['permission_name' => 'store_read'],
            ['permission_name' => 'store_update'],
            ['permission_name' => 'store_delete'],
            ['permission_name' => 'users_create'],
            ['permission_name' => 'users_read'],
            ['permission_name' => 'users_update'],
            ['permission_name' => 'users_delete'],
            ['permission_name' => 'wishlist_create'],
            ['permission_name' => 'wishlist_read'],
            ['permission_name' => 'wishlist_update'],
            ['permission_name' => 'wishlist_delete'],
        ]);
    }
}
