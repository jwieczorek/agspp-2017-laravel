<?php

use Illuminate\Database\Seeder;

class TermsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('terms')->insert([
            ['name' => 'Default', 'slug' => 'default']
        ]);

        DB::table('term_taxonomy')->insert([
            ['taxonomy' => 'category', 'term_id' => 1]
        ]);

        DB::table('term_relationships')->insert([
            ['object_id' => 1, 'term_taxonomy_id' => 1]
        ]);
    }
}
