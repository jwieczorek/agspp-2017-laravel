<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Add default user.
        $this->call(UsersTableSeeder::class);
        // Add user roles.
        $this->call(UserRolesTableSeeder::class);
        // Assign user to roles.
        $this->call(UsersRolesTableSeeder::class);
        // Seed api keys.
        $this->call(ApiTableSeeder::class);
        // Seed shop configs.
        $this->call(ShopsConfigTableSeeder::class);
        // Seed discounts.
        $this->call(DiscountsTableSeeder::class);
        // Seed product links.
        $this->call(LinksTableSeeder::class);
        // Seed spot prices.
        $this->call(SpotPricesTableSeeder::class);
        // Seed user permissions
        $this->call(UserPermissionsTableSeeder::class);
        // Seed content table.
        $this->call(ContentTableSeeder::class);
        // Seed terms table.
        $this->call(TermsTableSeeder::class);
    }
}
