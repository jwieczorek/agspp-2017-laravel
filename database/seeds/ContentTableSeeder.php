<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class ContentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * Create seed.
         */
        $faker = Faker::create();

        /**
         * Create posts.
         */
        foreach(range(1,20) as $index):
            DB::table('content')->insert([
                'author' => 1,
                'title' => $faker->sentence(3),
                'slug' => str_slug($faker->sentence(3), '-'),
                'guide' => $faker->url(),
                'content' => implode(' ', $faker->paragraphs()),
                'excerpt' => $faker->paragraph(),
                'type' => 'post',
                'status' => 'publish'
            ]);
        endforeach;

        /**
         * Create pages.
         */
        foreach(range(1,20) as $index):
            DB::table('content')->insert([
                'author' => 1,
                'title' => $faker->sentence(3),
                'slug' => str_slug($faker->sentence(3), '-'),
                'guide' => $faker->url(),
                'content' => implode(' ', $faker->paragraphs()),
                'excerpt' => $faker->paragraph(),
                'type' => 'page',
                'status' => 'publish'
            ]);
        endforeach;

        /**
         * Create products.
         */
        foreach(range(1,20) as $index):
            DB::table('content')->insert([
                'author' => 1,
                'title' => $faker->sentence(3),
                'slug' => str_slug($faker->sentence(3), '-'),
                'guide' => $faker->url(),
                'content' => implode(' ', $faker->paragraphs()),
                'excerpt' => $faker->paragraph(),
                'type' => 'product',
                'status' => 'publish',
                'bulk_pricing' => 0,
                'price' => serialize([['Price' => $faker->randomFloat()]])
            ]);
        endforeach;
    }
}
