<?php

/**
 * Get redirect url from query parameter
 * or posted value.
 *
 * @return mixed|null
 */
function get_redirect_url()
{
    /**
     * Get and clean redirect url.
     */
    $redirect = isset($_REQUEST['redirect']) ? urldecode($_REQUEST['redirect']) : null;
    $redirect_cleaned = filter_var($redirect, FILTER_SANITIZE_URL);

    /**
     * Get and return redirect url.
     */
    if($redirect_cleaned):
        return $redirect_cleaned;
    endif;

    return null;
}