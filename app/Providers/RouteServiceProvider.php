<?php

namespace Agspp\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'Agspp\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        /**
         * Admin routes.
         */
        Route::prefix('admin_'.env('ADMIN_URL_ENDING'))
            ->middleware('admin')
            ->namespace($this->namespace)
            ->group(base_path('routes/admin.php'));

        /**
         * Public web routes.
         */
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        /**
         * API Version 1
         */
        Route::prefix('api/v1')
            ->middleware('api1')
            ->namespace($this->namespace)
            ->group(base_path('routes/api1.php'));

        /**
         * API Version 2
         */
        Route::prefix('api/v2')
           ->middleware('api2')
           ->namespace($this->namespace)
           ->group(base_path('routes/api2.php'));
    }
}
