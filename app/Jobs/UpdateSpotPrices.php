<?php

namespace Agspp\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use SoapClient;
use Agspp\Data\SpotPrices;

class UpdateSpotPrices implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        /**
         * Get spot prices.
         */
        $prices = $this->_get_spot_prices();

        /**
         * Loop through prices and update them.
         */
        foreach($prices as $metal => $price):
            $spotprice = SpotPrices::where('metal', $metal)->first();
            $spotprice->update(
                [
                    'ask' => $price,
                    'bid' => null,
                ]
            );
        endforeach;

        /**
         * Remove the prices variable.
         */
        unset($prices);
    }

    /**
     * Get and return spot prices.
     *
     * @return array
     */
    private function _get_spot_prices()
    {
        return [
            'gold' => $this->_get_gold_price(),
            'silver' => $this->_get_silver_price(),
            'platinum' => $this->_get_platinum_price(),
            'palladium' => $this->_get_palladium_price()
        ];
    }

    /**
     * Get gold spot price.
     *
     * @return mixed
     */
    private function _get_gold_price()
    {
        $client = new SoapClient(config('spotprice.urls.gold'));
        $price = str_replace(',', '', $client->GetCurrentGoldPrice(config('spotprice.creds'))->GetCurrentGoldPriceResult->string[0]);
        return $price + (float) config('spotprice.overages.gold');
    }

    /**
     * Get silver spot price.
     *
     * @return mixed
     */
    private function _get_silver_price()
    {
        $client = new SoapClient(config('spotprice.urls.silver'));
        $price = str_replace(',', '', $client->GetCurrentSilverPrice(config('spotprice.creds'))->GetCurrentSilverPriceResult);
        return $price + (float) config('spotprice.overages.silver');
    }

    /**
     * Get platinum spot price.
     *
     * @return mixed
     */
    private function _get_platinum_price()
    {
        $client = new SoapClient(config('spotprice.urls.platinum'));
        $price = (float) substr(str_replace(',', '', $client->GetCurrentPlatinumPrice(config('spotprice.creds'))->GetCurrentPlatinumPriceResult), 0, 6);
        return $price + (float) config('spotprice.overages.platinum');
    }

    /**
     * Get palladium spot price.
     *
     * @return mixed
     */
    private function _get_palladium_price()
    {
        $client = new SoapClient(config('spotprice.urls.palladium'));
        $price = (float) substr(str_replace(',', '', $client->GetCurrentPalladiumPrice(config('spotprice.creds'))->GetCurrentPalladiumPriceResult), 0, 6);
        return $price + (float) config('spotprice.overages.palladium');
    }
}
