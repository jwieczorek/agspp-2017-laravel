<?php

namespace Agspp\Jobs;

use Agspp\Data\MessageLog;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class LogMessage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $data;

    /**
     * Create a new job instance.
     *
     */
    public function __construct($data)
    {
        $this->data = (array)$data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $log_array = [
            'message' => $this->data[0],
            'type' => $this->data[1]
        ];
        $log = new MessageLog();
        $log->fill($log_array);
        $log->save();
    }
}
