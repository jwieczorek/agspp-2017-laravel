<?php

namespace Agspp\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Agspp\Data\SiteActivity;

class LogSiteActivity implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $data;

    /**
     * LogSiteActivity constructor.
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = (array) $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $activity = new SiteActivity;
        $activity->fill($this->data);
        $activity->save();
    }
}