<?php

namespace Agspp\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use Agspp\Data\LoginPin;
use Agspp\Mail\SendLoginPin;

use Agspp\Data\MessageLog;

class DoLoginPin implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Delivery method.
     *
     * @var string $method
     */
    public $method;

    /**
     * Phone number.
     *
     * @var $phone_number
     */
    public $phone_number;

    /**
     * Email address.
     *
     * @var $email
     */
    public $email;

    /**
     * User's ID.
     *
     * @var $user_id
     */
    public $user_id;

    /**
     * Pin number.
     *
     * @var $pin
     */
    public $pin;

    /**
     * DoLoginPin constructor.
     *
     * @param $delivery_method
     * @param $user_id
     * @param $phone_number
     * @param $email
     * @param $pin
     */
    public function __construct($delivery_method, $user_id, $phone_number, $email, $pin)
    {
        $this->method = $delivery_method;
        $this->user_id = $user_id;
        $this->phone_number = $phone_number;
        $this->email = $email;
        $this->pin = $pin;
    }

    /**
     * Execute the job.
     *
     * @param LoginPin $model
     */
    public function handle(LoginPin $model)
    {
        /**
         * Save token to database.
         */
        $model->fill([
            'user_id' => $this->user_id,
            'number' => $this->pin
        ]);
        $model->save();

        /**
         * Deliver token.
         */
        switch ($this->method):
            case 'email':
                $this->_send_email($this->pin);
                break;
            case 'sms':
                $this->_send_sms($this->pin);
                break;
        endswitch;
    }

    /**
     * Send email to user.
     *
     * @param $pin
     */
    private function _send_email($pin)
    {
        /**
         * Email message.
         */
        Mail::to($this->email)->send(new SendLoginPin($pin));

        /**
         * Log message.
         */
        $log = new MessageLog();
        $log->fill([
            'message' => 'sending email token to: '. $this->email,
            'type' => 'info'
        ]);
        $log->save();
    }

    /**
     * Send sms to user.
     *
     * @param $pin
     */
    private function _send_sms($pin)
    {
        /**
         * Send sms message.
         */
        new \Agspp\Models\Sms\Send($this->phone_number, sprintf(config('messages.sms.loginCode'), $pin));

        /**
         * Log message.
         */
        $log = new MessageLog();
        $log->fill([
            'message' => 'sending sms token to: '. $this->phone_number,
            'type' => 'info'
        ]);
        $log->save();
    }
}
