<?php

namespace Agspp\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendLoginPin extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $token;
    public $subject;

    /**
     * Create a new message instance.
     */
    public function __construct($token)
    {
        $this->token = $token;
        $this->subject = 'Your AGSPP Login PIN!';
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.send-login-pin')
            ->with([
                'token' => $this->token
            ])->subject($this->subject);
    }
}
