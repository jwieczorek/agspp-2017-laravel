<?php

namespace Agspp\Models;
use Agspp\Jobs\LogMessage;

/**
 * Class Log
 *
 * Message logging wrapper.
 *
 * @package Agspp\Models
 */
class Log
{
    public function __construct($message='', $type='info')
    {
        dispatch(new LogMessage([
            $message,
            $type
        ]));
    }
}