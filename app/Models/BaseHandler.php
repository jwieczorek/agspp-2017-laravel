<?php
/**
 * Created by PhpStorm.
 * User: joshuawieczorek
 * Date: 5/7/17
 * Time: 8:24 AM
 */

namespace Agspp\Models;
use Illuminate\Http\Request;


class BaseHandler
{
    /**
     * Generate meta array.
     *
     * @param $meta_array
     * @return array
     */
    protected function generate_meta($meta_array)
    {
        $return_meta = [];
        /**
         * Loop through meta items and
         * throw into local array.
         */
        foreach ($meta_array as $meta):
            $return_meta[$meta['meta_key']] = $meta['meta_value'];
        endforeach;

        // Return filled meta array.
        return $return_meta;
    }

    /**
     * Generate links array.
     *
     * @param $links_array
     * @return array
     */
    protected function generate_links($links_array)
    {
        $return_links = [];
        /**
         * Loop through meta items and
         * throw into local array.
         */
        foreach ($links_array as $link):
            $return_links[$link['title']] = $link['link'];
        endforeach;

        // Return filled meta array.
        return $return_links;
    }

    /**
     * Generate categories array.
     *
     * @param $categories_array
     * @return array
     */
    protected function generate_categories($categories_array)
    {
        $return_categories = [];
        /**
         * Loop through meta items and
         * throw into local array.
         */
        foreach ($categories_array as $category):
            $return_categories[$category['id']] = $category['name'];
        endforeach;

        // Return filled meta array.
        return $return_categories;
    }
}