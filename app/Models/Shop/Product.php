<?php

namespace Agspp\Models\Shop;

use Illuminate\Http\Request;
use Agspp\Data\Product as ProductModel;
use Agspp\Data\Product as Entity;
use Agspp\Jobs\LogSiteActivity;

class Product extends \Agspp\Models\BaseHandler
{
    /**
     * Product model.
     *
     * @var ProductModel $product
     */
    protected $product;

    /**
     * Product constructor.
     *
     * @param ProductModel $product
     */
    public function __construct(ProductModel $product)
    {
        $this->product = $product;
    }

    private function _get_product($product_id)
    {
        if(is_numeric($product_id)):
            return $this->product->where('id', $product_id)->first();
        endif;

        return $this->product->where('slug', $product_id)->first();
    }

    /**
     * Get product by id or slug.
     *
     * @param $id
     * @return mixed
     */
    public function get($id)
    {
        $product = $this->_get_product($id);
        $product->meta = $this->generate_meta($product->product_meta);
        $product->links = $this->generate_links($product->product_links);
        $product->categories = $this->generate_categories($product->product_categories);
        unset($product->product_categories);
        unset($product->product_meta);
        unset($product->product_links);
        return $product;
    }

    /**
     * Insert product into database.
     *
     * @param Request $request
     * @return bool|mixed
     */
    public function create(Request $request)
    {
        /**
         * Create entity.
         */
        $entity = (array) new Entity($request->all());
        $this->product->fill($entity);

        /**
         * Return true on successful save.
         */
        if(true === $this->product->save()):
            return $this->product->id;
        endif;

        // Otherwise return false.
        return false;
    }

    /**
     * Update product in database.
     *
     * @param Request $request
     * @param String $product_id
     * @return bool
     */
    public function update(Request $request, $product_id)
    {
        /**
         * Create entity and update properties.
         */
        $product = $this->_get_product($product_id);
        $product->fill($request->all());

        /**
         * Return true on successful save.
         */
        if(true === $product->save()):
            return true;
        endif;

        // Otherwise return false.
        return false;
    }
}