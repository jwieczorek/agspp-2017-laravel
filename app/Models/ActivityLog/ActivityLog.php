<?php

namespace Agspp\Models\ActivityLog;
use Illuminate\Http\Request;

class ActivityLog
{
    /**
     * Return array of log data for
     * activity log.
     *
     * @param Request $request
     * @param $user_id
     * @param $referrer
     * @param $status
     * @return array
     */
    public static function data(Request $request, $user_id, $referrer, $status)
    {
        return [
            'user_id' => $user_id,
            'referrer' => $referrer,
            'action' => $request->getUri(),
            'ip' => $request->ip(),
            'session_id' => ($referrer === 'api') ? 'api' : $request->session()->getId(),
            'user_agent' => $request->header('user-agent'),
            'status' => $status
        ];
    }
}