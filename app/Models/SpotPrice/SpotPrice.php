<?php

namespace Agspp\Models\SpotPrice;
use Agspp\Data\SpotPrices as Model;

class SpotPrice
{
    /**
     * Get all spot prices.
     */
    public function get()
    {
        $db_results = Model::get();
        $spot_prices = [];

        /**
         * Loop through db results as set
         * in local array.
         */
        foreach ($db_results as $spotprice):
            $spot_prices[$spotprice['metal']] = number_format($spotprice['ask'], 2);
        endforeach;

        // Return spot price array
        return $spot_prices;
    }
}