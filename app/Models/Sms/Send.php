<?php

namespace Agspp\Models\Sms;

/**
 * Class Send
 *
 * Wrapper for Nexmo sms sender.
 *
 * @package Agspp\Models\Sms
 */
class Send
{
    /**
     * Send sms message constructor.
     *
     * @param string $phone_number
     * @param string $message
     */
    public function __construct($phone_number, $message)
    {
        /**
         * Send sms.
         */
        $nexmo = app('Nexmo\Client');
        $nexmo->message()->send([
            'to' => $phone_number,
            'from' => env('NEXMO_FROM'),
            'text' => $message
        ]);
    }
}