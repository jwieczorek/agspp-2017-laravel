<?php

namespace Agspp\Models\ApiKey;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Agspp\Models\BaseHandler;
use Agspp\Data\ApiKey as Model;

class ApiKey extends BaseHandler
{
    /**
     * Get all api keys.
     */
    public function get()
    {

    }

    /**
     * Create api key.
     */
    public function create()
    {

    }

    /**
     * Update api key
     */
    public function update()
    {

    }

    /**
     * Validate API request.
     *
     * @param Request $request
     * @return bool
     */
    public function validate(Request $request)
    {
        $key = Model::where([
            'username' => $request->header('Api-Login'),
            'password' => $request->header('Api-Password'),
            'key' => $request->header('Api-Key')
        ])->first();

        /**
         * If user has permission then
         * throw job to user id and log activity.
         */
        if($key && $key->user->api_access == 1) :
            Auth::loginUsingId($key->user->id);
            return true;
        endif;

        /**
         * Return false.
         */
        return false;
    }
}