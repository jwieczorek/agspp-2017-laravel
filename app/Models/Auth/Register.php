<?php

namespace Agspp\Models\Auth;

use Illuminate\Http\Request;
use Agspp\Data\User;
use Agspp\Models\Log;

class Register
{
    /**
     * Create user.
     *
     * @param Request $request
     * @return bool
     */
    public function create(Request $request)
    {
        /**
         * Create user data.
         */
        $new_user_data = [
            'first_name' => $request->input('fname'),
            'last_name' => $request->input('lname'),
            'username' => $request->input('username'),
            'email' => $request->input('email'),
            'phone' => $request->input('phone'),
        ];

        /**
         * Save user.
         */
        $user = new User;
        $user->fill($new_user_data);
        return $user->save();
    }
}