<?php

namespace Agspp\Models\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Agspp\Data\LoginSession;
use Agspp\Data\User;
use Agspp\Data\LoginPin;
use Agspp\Models\Log;
use Illuminate\Support\Facades\Cookie;

class Login
{
    /**
     * User object.
     *
     * @var User
     */
    protected $user;

    /**
     * Login constructor.
     *
     * @param User $user
     */
    public function __construct(Request $request, User $user)
    {
        $this->user = $user;
    }

    /**
     * Try to send user code.
     *
     * @param Request $request
     * @return bool
     */
    public function try_send_code(Request $request)
    {
        /**
         * Set delivery method.
         */
        $delivery_method = $request->has('send-email') ? 'email' : ($request->has('send-sms') ? 'sms' : 'email');

        /**
         * Get and clean user login.
         */
        $user_login = $request->input('user_login');

        /**
         * Get user.
         */
        $user = $this->user->findUserByUserLogin($user_login);

        /**
         * No user log message and return false.
         */
        if(!$user):
            /**
             * Log error message.
             */
            new Log("No user found for: {$user_login}", 'error');
            return false;
        endif;

        /**
         * Make sure user is active.
         */
         if(0 === $user->active):
            new Log("User {$user_login} not active!", 'error');
            return false;
        endif;

        /**
         * See if user has login token and delete it.
         */
        if($user->loginPin):
            $user->deleteLoginPin($user->id);
        endif;

        /**
         * Create login token.
         */
        return $user->createLoginPin($user, $delivery_method);
    }

    /**
     * Check if pin is expired.
     *
     * @param $key
     * @return bool
     */
    public function verify_url_key($key)
    {

        /**
         * URL pin time.
         */
        $url_pin = decrypt($key);



        /**
         * Check if expired.
         */
        if(!$this->_validate_time($url_pin[1])):
            return false;
        endif;

        /**
         * Otherwise return true.
         */
        return true;
    }

    /**
     * Verify user posted pin.
     *
     * @param Request $request
     * @param string $key
     * @return bool
     */
    public function verify_pin(Request $request, $key)
    {
        /**
         * Get compile pin number.
         */
        $pin = implode($request->input('login_pin'));

        /**
         * Verify it is 6 digits.
         */
        if(6 !== strlen($pin)):
            return false;
        endif;

        /**
         * Try to get pin from database and verify with url key.
         */
        $database_pin = LoginPin::where(['number'=>$pin])->first();

        /**
         * No database pin return false.
         */
        if(!$database_pin):
            return false;
        endif;

        /**
         * Check if expired and login.
         */
        if(!$this->_validate_time(strtotime($database_pin->created_at))):
            return false;
        endif;

        /**
         * Validate that key and pin are same.
         */
        if($database_pin->number == decrypt($key)[0]):
            $user = User::find($database_pin->user_id);
            Auth::login($user);
            $this->_do_login_session($request, $user->id, $user);
            $database_pin->delete();
            return true;
        endif;

        /**
         * Otherwise return false.
         */
        return false;
    }

    /**
     * Check for remember cookie and login
     * based on cookie.
     *
     * @param Request $request
     * @return bool|void
     */
    public function remember_cookie_check(Request $request)
    {
        /**
         * If logged in stop.
         */
        if(Auth::check()):
            return;
        endif;

        /**
         * No cookie then stop.
         */
        if(!isset($_COOKIE[config('site.cookies.remember')])):
            return;
        endif;

        /**
         * Get cookie values.
         */
        //dd($request->cookie(config('site.cookies.remember')));
        $cookie = unserialize($request->cookie(config('site.cookies.remember')));

        /**
         * Find login session for cookie.
         */
        $session = LoginSession::where([
            'user_id' => $cookie['user'],
            'session_id' => $cookie['session_id']
        ])->first();

        if(!$session):
            return;
        endif;

        /**
         * Generate new cookie.
         */
        $new_cookie = serialize([
            'user'=>$cookie['user'],
            'user-agent'=>$request->header('user-agent'),
            'session_id'=>session()->getId()
        ]);

        /**
         * Forget old cookie and reset with new.
         */
        Cookie::forget(config('site.cookies.remember'));
        Cookie::queue(config('site.cookies.remember'), $new_cookie, (1440*14));

        /**
         * Log user in.
         */
        $user = User::find($cookie['user']);
        Auth::login($user);
        session(['usr_name'=>$user->username]);
        session(['usr_fname'=>$user->first_name]);
        session(['usr_lname'=>$user->last_name]);
        session(['usr_email'=>$user->email]);

        /**
         * Save login session.
         */
        $session->fill([
            'user_id' => $cookie['user'],
            'cookie' => encrypt($new_cookie),
            'session_id' => session()->getId(),
            'user_agent' => $request->header('user-agent'),
            'expiration' => strtotime("+14days", time())
        ]);
        $session->save();
    }

    /**
     * Do remember me.
     *
     * @param Request $request
     * @param int $user_id
     * @param User $user
     */
    private function _do_login_session(Request $request, $user_id, $user)
    {
        /**
         * User agent.
         */
        $user_agent = $request->header('user-agent');

        /**
         * Set cookie.
         */
        $cookie = null;

        /**
         * Check if remember me was set.
         */
        if($request->input('remember_me')):
            $cookie = serialize(['user'=>$user_id, 'user-agent'=>$user_agent, 'session_id'=>session()->getId()]);
            Cookie::queue(config('site.cookies.remember'), $cookie, (1440*14));
        endif;

        /**
         * Set user's name session.
         */
        session(['usr_name'=>$user->username]);
        session(['usr_fname'=>$user->first_name]);
        session(['usr_lname'=>$user->last_name]);
        session(['usr_email'=>$user->email]);

        /**
         * Save login session.
         */
        /**
         * Create login session.
         */
        $login_session = new LoginSession;
        $login_session->fill([
            'user_id' => $user_id,
            'cookie' => encrypt($cookie),
            'session_id' => session()->getId(),
            'user_agent' => $user_agent,
            'expiration' => strtotime("+14days", time())
        ]);
        $login_session->save();

    }

    /**
     * Validate time.
     *
     * @param $time
     * @return bool
     */
    private function _validate_time($time)
    {
        /**
         * Expiration time.
         */
        $expiry_time = strtotime("-10minutes", time());

        /**
         * Check if expired.
         */
        if($time <= $expiry_time):
            return false;
        endif;

        /**
         * Otherwise return true.
         */
        return true;
    }
}
