<?php

namespace Agspp\Data;

use Illuminate\Database\Eloquent\Model;

class MessageLog extends Model
{
    /**
     * Set timestamps to false.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Table.
     *
     * @var string $table
     */
    protected $table = 'logs';

    /**
     * Fillable fields.
     *
     * @var array $fillable
     */
    protected $fillable = [
        'message',
        'type',
        'time'
    ];
}
