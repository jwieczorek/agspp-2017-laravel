<?php

namespace Agspp\Data;

use Illuminate\Database\Eloquent\Model;

class ShopConfigs extends Model
{
    public $timestamps = false;

    /**
     * Return correctly formatted config entry.
     */
    public function setConfigKeyAttribute($value)
    {
        return preg_replace('[\s]', '_', $value);
    }

    /**
     * Shop for config entry.
     */
    public function shop()
    {
        return $this->hasOne(Shop::class);
    }
}
