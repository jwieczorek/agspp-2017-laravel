<?php

namespace Agspp\Data;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Http\Request;
use Agspp\Models\Log;
use Agspp\Jobs\DoLoginPin;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'username', 'email', 'phone', 'cookie'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'active'
    ];

    /**
     * Get user's roles.
     *
     * @return array [user's roles]
     */
    public function roles()
    {
        return $this->belongsToMany(UserRole::class, 'users_roles', 'user_id', 'id');
    }

    /**
     * Get user's meta.
     *
     * @return array [all user's meta]
     */
    public function user_meta()
    {
        return $this->hasMany(UserMeta::class);
    }

    /**
     * Get user's api keys.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function api_keys()
    {
        return $this->hasMany(ApiKey::class);
    }

    /**
     * Get user's permissions.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function permissions()
    {
        return $this->hasMany(UserPermissions::class);
    }

    /**
     * Check if user has role.
     *
     * @param $role_to_check
     * @return bool
     */
    public function hasRole($role_to_check)
    {
        $users_roles = $this->roles;

        foreach($users_roles as $role):
            if($role_to_check == $role->role_name):
                return true;
            endif;
        endforeach;

        return false;
    }

    /**
     * Check if user has permission.
     *
     * @param $perm_to_check
     * @return bool
     */
    public function hasPermission($perm_to_check)
    {
        $users_perms = $this->permissions;

        foreach($users_perms as $perm):
            if($perm_to_check == $perm->permission_name):
                return true;
            endif;
        endforeach;

        return false;
    }


    /**
     * Find user by login.
     *
     * @param $user_login
     * @return mixed
     */
    public function findUserByUserLogin($user_login)
    {
        return $this->where('username', $user_login)
            ->orWhere('email', $user_login)
            ->orWhere('phone', $user_login)
            ->first();
    }

    public function loginPin()
    {
        return $this->hasOne(LoginPin::class, 'user_id');
    }

    /**
     * Get login token.
     *
     * @return mixed|bool
     */
    public function getLoginPin()
    {
        /**
         * Check if user has login token.
         */
        if(null == $this->loginPin):
            /**
             * Log error and return false.
             */
            new Log("No login token for user:{$this->id}", 'error');
            return false;
        endif;

        /**
         * Log message and return token,
         */
        new Log("Returning login token for user:{$this->id}");
        return $this->loginPin->number;
    }

    /**
     * Create login token and send to user.
     *
     * @param User $user
     * @param $delivery_method
     * @return string
     */
    public function createLoginPin(User $user, $delivery_method)
    {
        /**
         * Generate login token.
         */
        new Log("Generating pin for {$user->id}.");

        $pin = mt_rand(100000, 999999);

        new Log("Pin generated for {$user->id}.");

        /**
         * Set pin in session.
         */
        session(['login_pin' => $this->_generate_url_key($pin)]);

        /**
         * Dispatch login token.
         */
        dispatch(new DoLoginPin($delivery_method, $user->id, $user->phone, $user->email, $pin));

        /**
         * Return pin number.
         */
        return session('login_pin');
    }

    /**
     * Verify user's login token.
     *
     * @param Request $request
     * @return bool
     */
    public function verifyLoginPin(Request $request)
    {
        return true;
    }

    /**
     * Delete existing login token.
     *
     * @param $user_id
     */
    public function deleteLoginPin($user_id)
    {
        new Log("Deleting login token for: {$user_id}");
        $pin = LoginPin::where('user_id', $user_id);
        $pin->delete();
    }

    /**
     * Generate url key/pin parameter.
     *
     * @param $pin
     * @return mixed
     */
    private function _generate_url_key($pin)
    {
        new Log("Creating session key/pin.");
        return encrypt([$pin,time()]);
    }
}
