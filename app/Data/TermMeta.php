<?php

namespace Agspp\Data;

use Illuminate\Database\Eloquent\Model;

class TermMeta extends Model
{
    protected $table = 'term_meta';
}
