<?php

namespace Agspp\Data;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class ApiKey extends Model
{
    protected $table = 'api_keys';

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
