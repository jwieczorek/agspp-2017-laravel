<?php

namespace Agspp\Data;

use Illuminate\Database\Eloquent\Model;

class TermTaxonomy extends Model
{
    public $timestamps = false;

    protected $table = 'term_taxonomy';

    protected $fillable = [
        'term_id', 'taxonomy'
    ];
}
