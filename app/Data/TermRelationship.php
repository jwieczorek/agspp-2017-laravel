<?php

namespace Agspp\Data;

use Illuminate\Database\Eloquent\Model;

class TermRelationship extends Model
{
    /**
     * Turn off timestamps.
     *
     * @var bool
     */
    public $timestamps = false;

    protected $table = 'term_relationships';

    protected $fillable = [
        'object_id',
        'term_taxonomy_id'
    ];
}
