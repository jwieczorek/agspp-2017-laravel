<?php

namespace Agspp\Data;

use Illuminate\Database\Eloquent\Model;

class SiteActivity extends Model
{
    protected $table = 'site_activity';

    protected $fillable = [
        'user_id',
        'action',
        'referrer',
        'ip',
        'session_id',
        'user_agent',
        'status'
    ];
}
