<?php

namespace Agspp\Data;

use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{
    /**
     * Turn off timestamps.
     *
     * @var bool $timestamps
     */
    public $timestamps = false;

    /**
     * Model table.
     *
     * @var string $table
     */
    protected $table = 'discounts';

    /**
     * Product discount.
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class, 'id');
    }
}
