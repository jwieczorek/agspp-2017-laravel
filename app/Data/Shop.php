<?php

namespace Agspp\Data;

use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{
    /**
     * Shop's configs.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function configs()
    {
        return $this->hasMany(ShopConfigs::class);
    }

    /**
     * Shop's products.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->hasMany(Product::class);
    }
}
