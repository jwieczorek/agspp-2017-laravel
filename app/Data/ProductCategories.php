<?php

namespace Agspp\Data;

use Illuminate\Database\Eloquent\Model;

class ProductCategories extends Model
{
    protected $table = 'product_categories';

    /**
     * Products
     */
    public function products()
    {
        return $this->belongsToMany(Product::class, 'products_categories', 'product_id', 'category_id');
    }
}
