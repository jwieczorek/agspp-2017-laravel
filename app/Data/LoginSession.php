<?php

namespace Agspp\Data;

use Illuminate\Database\Eloquent\Model;

class LoginSession extends Model
{
    protected $table = 'login_sessions';
    protected $fillable = [
        'user_id',
        'cookie',
        'session_id',
        'user_agent',
        'expiration',
        'created_at',
        'updated_at'
    ];
}
