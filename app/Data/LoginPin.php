<?php

namespace Agspp\Data;

use Illuminate\Database\Eloquent\Model;

class LoginPin extends Model
{
    protected $table = 'login_pins';

    protected $fillable = [
        'user_id', 'number'
    ];
}
