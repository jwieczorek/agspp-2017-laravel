<?php

namespace Agspp\Data;

use Illuminate\Database\Eloquent\Model;

class ProductLinks extends Model
{
    protected $table = 'product_links';

    public function product()
    {
        return $this->hasOne(Product::class);
    }
}
