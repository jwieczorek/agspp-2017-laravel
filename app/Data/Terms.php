<?php

namespace Agspp\Data;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Agspp\Jobs\LogMessage;

class Terms extends Model
{
    /**
     * Turn off timestamps.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Terms table.
     *
     * @var string $table
     */
    protected $table = 'terms';

    /**
     * Terms table.
     *
     * @var string $_table_terms
     */
    private $_table_terms;

    /**
     * Taxonomy table.
     *
     * @var string $_table_taxonomy
     */
    private $_table_taxonomy;

    /**
     * Term relationship table.
     *
     * @var string $_table_term_relationships
     */
    private $_table_term_relationships;

    /**
     * Fillable fields.
     *
     * @var array $fillable
     */
    protected $fillable = [
        'name', 'slug'
    ];

    /**
     * Terms constructor.
     */
    public function __construct()
    {
        $database = env('DB_DATABASE');
        $this->_table_terms = sprintf("%s.terms", $database);
        $this->_table_taxonomy = sprintf("%s.term_taxonomy", $database);
        $this->_table_term_relationships = sprintf("%s.term_relationships", $database);
    }

    /**
     *
     * @param int $object_id
     * @param string $term_name
     * @return mixed
     */
    public function get_terms($object_id=0, $term_name='')
    {
        /**
         * Get object term by name
         */
        if($object_id > 0 && '' !== $term_name):
            return $this->_get_content_terms_by($object_id, $term_name);
        /**
         * Get all object terms.
         */
        elseif($object_id > 0):
            return $this->_get_content_terms($object_id);
        endif;

        /**
         * Get all terms without object id.
         */
        return $this->_get_content_terms();
    }

    /**
     * See if term exists.
     *
     * @param string $term_name
     * @param string $term_type
     * @return bool
     */
    public function term_exists($term_name='', $term_type='')
    {
        $sql = "select count(tm.name) as count
	            from {$this->_table_terms} tm
	            inner join {$this->_table_taxonomy} tx
	            on tm.id = tx.term_id
	            where tm.name = '{$term_name}'
	            and tx.taxonomy = '{$term_type}'";
        /**
         * If no term was returned,
         */
        if(DB::select($sql)[0]->count === 0):
            return false;
        endif;

        /**
         * Term was present return true.
         */
        return true;
    }

    /**
     * See if object has a particular term.
     *
     * @param $object_id
     * @param string $term_name
     * @param string $term_type
     * @return bool
     */
    public function object_term_exists($object_id, $term_name='', $term_type='')
    {
        $sql = "select count(tm.name) as count                
                from {$this->_table_terms} tm
                inner join {$this->_table_taxonomy} tx
                on tm.id = tx.term_id
                inner join {$this->_table_term_relationships} tr
                on tr.term_taxonomy_id = tx.id
                where tr.object_id = {$object_id}
                and tx.taxonomy = '{$term_type}'
                and tm.name = '{$term_name}'";
        /**
         * If no term was returned,
         */
        if(DB::select($sql)[0]->count === 0):
            return false;
        endif;

        /**
         * Term was present return true.
         */
        return true;
    }

    public function create_term_taxonomy($term_name, $term_slug, $term_type)
    {
        /**
         * If term exists return false.
         */
        if($this->term_exists($term_name, $term_type)):
            dispatch(new LogMessage([
                "Taxonomy {$term_name} not created: taxonomy already exists.",
                'error'
            ]));
            return false;
        endif;

        /**
         * If term type is not in available terms
         * return false.
         */
        $available_terms = config('taxonomies');
        if(!in_array($term_type, $available_terms)):
            dispatch(new LogMessage([
                "Taxonomy {$term_name} not created: taxonomy {$term_type} not allowed.",
                'error'
            ]));
            return false;
        endif;

        /**
         * Otherwise create term taxonomy.
         */

        /**
         * Create term.
         */
        $this->fill([
            'name' => $term_name,
            'slug' => $term_slug
        ]);
        $this->save();

        /**
         * Link term taxonomy.
         */
        $tax = new TermTaxonomy;
        $tax->fill([
            'term_id' => $this->id,
            'taxonomy' => $term_type
        ]);
        $tax->save();

        dispatch(new LogMessage([
            "Taxonomy {$term_name} was created.",
            'info'
        ]));

        /**
         * Return true.
         */
        return $tax->id;
    }

    /**
     * Link object to taxonomy.
     *
     * @param $object_id
     * @param $taxonomy_id
     */
    public function link_to_taxonomy($object_id, $taxonomy_id)
    {
        /**
         * Search for relationship.
         */
        $exists = TermRelationship::where([
            'object_id' => $object_id,
            'term_taxonomy_id' => $taxonomy_id
        ])->first();

        /**
         * No relationship then assign relationship.
         */
        if(!$exists):
            $rel = new TermRelationship;
            $rel->fill([
                'object_id' => $object_id,
                'term_taxonomy_id' => $taxonomy_id
            ]);
            $rel->save();
            dispatch(new LogMessage([
                "Object linked, obj:{$object_id} | tax:{$taxonomy_id}",
                'info'
            ]));
        endif;

        /**
         * Relationship exists log error.
         */
        dispatch(new LogMessage([
            "Object not linked, link already exists obj:{$object_id} | tax:{$taxonomy_id}",
            'error'
        ]));
    }

    /**
     * Get all available taxonomies.
     *
     * @return array
     */
    public function get_taxonomies()
    {
        return config('taxonomies');
    }

    /**
     * Get all of a product's terms.
     *
     * @param $object_id
     * @return mixed
     */
    private function _get_content_terms($object_id=false)
    {
        /**
         * If no term was passed get all terms.
         */
        if(false === $object_id):
            $sql = "select tm.name, tx.taxonomy
	            from {$this->_table_terms} tm
	            inner join {$this->_table_taxonomy} tx
	            on tm.id = tx.term_id";
            return DB::select($sql);
        endif;

        /**
         * Otherwise get term for object id.
         */
        $sql = "select tm.name, tm.slug, tx.taxonomy
	            from {$this->_table_terms} tm
	            inner join {$this->_table_taxonomy} tx
	            on tm.id = tx.term_id
	            inner join {$this->_table_term_relationships} tr
	            on tr.term_taxonomy_id = tx.id
	            where tr.object_id = '{$object_id}';";
        return DB::select($sql);
    }

    /**
     * Get specific product terms.
     *
     * @param $object_id
     * @param $term_name
     * @return mixed
     */
    private function _get_content_terms_by($object_id, $term_name)
    {
        $sql = "select tm.name, tm.slug, tx.taxonomy
	            from {$this->_table_terms} tm
	            inner join {$this->_table_taxonomy} tx
	            on tm.id = tx.term_id
	            inner join {$this->_table_term_relationships} tr
	            on tr.term_taxonomy_id = tx.id
	            where tr.object_id = '{$object_id}' and tx.taxonomy = '{$term_name}';";
        return DB::select($sql);
    }
}
