<?php

namespace Agspp\Data;

use Illuminate\Database\Eloquent\Model;

class SpotPrices extends Model
{
    protected $table = 'spot_prices';

    protected $fillable = [
        'updated_at',
        'ask',
        'bid'
    ];
}
