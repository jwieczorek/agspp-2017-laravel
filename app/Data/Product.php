<?php
/**
 * Created by PhpStorm.
 * User: joshuawieczorek
 * Date: 4/29/17
 * Time: 10:09 AM
 */

namespace Agspp\Data;

class Product extends BaseModel
{
    protected $table = 'products';

    /**
     * Fields allowed to be filled.
     *
     * @var array $fillable
     */
    protected $fillable = [
        'id',
        'creator_id',
        'title',
        'slug',
        'guide',
        'price',
        'bulk_pricing',
        'short_description',
        'long_description',
        'featured_image',
        'type',
        'status'
    ];

    /**
     * Fields hidden from export.
     *
     * @var array $hidden
     */
    protected $hidden = [
        'access_role',
    ];

    /**
     * Product discount.
     */
    public function product_discount()
    {
        return $this->hasOne(Discount::class, 'product_id');
    }

    /**
     * Product's categories.
     */
    public function product_categories()
    {
        return $this->belongsToMany(ProductCategories::class, 'products_categories', 'product_id', 'category_id');
    }

    /**
     * Product's links.
     */
    public function product_links()
    {
        return $this->hasMany(ProductLinks::class);
    }

    /**
     * Product's links.
     */
    public function product_meta()
    {
        return $this->hasMany(ProductMeta::class);
    }

    /**
     * Serialize price.
     *
     * @param $price
     * @return string
     */
    public function setPriceAttribute($price)
    {
        $this->attributes['price'] = serialize($price);
    }

    /**
     * Un-serialize price.
     *
     * @param $price
     * @return mixed
     */
    public function getPriceAttribute($price)
    {
        return unserialize($price);
    }
}