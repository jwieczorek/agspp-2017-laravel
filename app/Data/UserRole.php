<?php

namespace Agspp\Models;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
    public $timestamps = false;

    protected $table = 'user_roles';

    /**
     * Get user's roles.
     */
    public function users()
    {
        $this->belongsToMany(\Agspp\Models\User::class);
    }
}
