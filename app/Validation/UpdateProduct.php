<?php

namespace Agspp\Validation;

use Illuminate\Validation\Rule;

class UpdateProduct
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'max:255',
            'slug' => 'alpha_dash|unique:products,slug|max:255',
            'price' => 'array',
            'bulk_pricing' => 'boolean',
            'type' => [
                Rule::in(['metal','other'])
            ],
            'status' => [
                Rule::in(['publish', 'draft', 'scheduled']),
            ],
            'categories' => 'array',
            'tags' => 'array'
        ];
    }

    /**
     * Set custom validation messages.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'title.aphpa_dash' => 'Must only contain the alphabet, numbers, underscores and dashes.',
            'title.max' => 'Must be shorter than 255 characters.',
            'slug.alpha_dash' => 'Must only contain the alphabet, numbers, underscores and dashes.',
            'slug.max' => 'Must be shorter than 255 characters.',
            'bulk_pricing' => 'Please state whether pricing is bulk or not',
            'short_description.required' => 'Please enter a short description.',
            'type.in' => 'Product type must be set to either metal or other.',
            'status.in' => 'Status must be set to either publish, draft or scheduled.'
        ];
    }
}