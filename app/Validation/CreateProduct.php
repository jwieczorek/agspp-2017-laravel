<?php

namespace Agspp\Validation;

use Illuminate\Validation\Rule;

class CreateProduct
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:255',
            'slug' => 'required|alpha_dash|unique:products,slug|max:255',
            'price' => 'required|array',
            'bulk_pricing' => 'required|boolean',
            'short_description' => 'required',
            'long_description' => 'present',
            'featured_image' => 'present',
            'type' => [
                'required',
                Rule::in(['metal','other'])
            ],
            'status' => [
                'required',
                Rule::in(['publish', 'draft', 'scheduled']),
            ],
            'categories' => 'present|array',
            'tags' => 'present|array'
        ];
    }

    /**
     * Set custom validation messages.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'title.required' => 'Please enter a title.',
            'title.aphpa_dash' => 'Must only contain the alphabet, numbers, underscores and dashes.',
            'title.max' => 'Must be shorter than 255 characters.',
            'slug.required' => 'Please enter a slug.',
            'slug.alpha_dash' => 'Must only contain the alphabet, numbers, underscores and dashes.',
            'slug.max' => 'Must be shorter than 255 characters.',
            'price.required' => 'Please enter a price.',
            'bulk_pricing' => 'Please state whether pricing is bulk or not',
            'short_description.required' => 'Please enter a short description.',
            'type.required' => 'Please enter a product type.',
            'type.in' => 'Product type must be set to either metal or other.',
            'status.in' => 'Status must be set to either publish, draft or scheduled.'
        ];
    }
}