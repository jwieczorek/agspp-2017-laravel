<?php

namespace Agspp\Validation;

use Illuminate\Validation\Rule;

class Login
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_login' => 'required'
        ];
    }

    /**
     * Set custom validation messages.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'user_id.required' => 'Please enter your Username, email or cell number.'
        ];
    }
}