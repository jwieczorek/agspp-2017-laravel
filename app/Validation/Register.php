<?php

namespace Agspp\Validation;

use Illuminate\Validation\Rule;

class Register
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fname' => 'required|min:2|max:50|regex:/^[a-zA-Z]+$/',
            'lname' => 'required|min:2|max:50|regex:/^[a-zA-Z+\s\']+$/',
            'username' => 'required|min:5|max:20|unique:users,username',
            'email' => 'required|email|unique:users,email',
            'phone' => 'required|regex:/^[0-9]{3}-[0-9]{3}-[0-9]{4}$/',
        ];
    }

    /**
     * Set custom validation messages.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'fname.required' => 'Please enter your first name!',
            'fname.regex' => 'Please enter only alphabetic characters!',
            'fname.min' => 'Your first name must be between 2 and 50 characters!',
            'fname.max' => 'Your first name must be between 2 and 50 characters!',

            'lname.required' => 'Please enter your last name!',
            'lname.regex' => 'Please enter only alphabetic characters and an apostrophe!',
            'lname.min' => 'Your last name must be between 2 and 50 characters!',
            'lname.max' => 'Your last name must be between 2 and 50 characters!',

            'username.required' => 'Please enter your username!',
            'username.min' => 'Your username must be between 5 and 50 characters!',
            'username.max' => 'Your username must be between 5 and 50 characters!',
            'username.unique' => 'Sorry, this username already exists in our system!',

            'email.required' => 'Please enter your email address!',
            'email.email' => 'Please enter a valid email address!',
            'email.unique' => 'Sorry, this email already exists in our system!',

            'phone.required' => 'Please a mobile phone number!',
            'phone.regex' => 'Please a valid US phone number!'
        ];
    }
}