<?php

namespace Agspp\Validation;


class LoginPin
{
    public function rules()
    {
        return [
            'login_pin.0' => 'required|numeric|digits:1',
            'login_pin.1' => 'required|numeric|digits:1',
            'login_pin.2' => 'required|numeric|digits:1',
            'login_pin.3' => 'required|numeric|digits:1',
            'login_pin.4' => 'required|numeric|digits:1',
            'login_pin.5' => 'required|numeric|digits:1',
        ];
    }

    public function messages()
    {
        return [
            'login_pin.0.required' => 'Please enter the first number.',
            'login_pin.0.numeric' => 'Please enter a number from 0-9.',
            'login_pin.1.required' => 'Please enter the second number.',
            'login_pin.1.numeric' => 'Please enter a number from 0-9.',
            'login_pin.2.required' => 'Please enter the third number.',
            'login_pin.2.numeric' => 'Please enter a number from 0-9.',
            'login_pin.3.required' => 'Please enter the fourth number.',
            'login_pin.3.numeric' => 'Please enter a number from 0-9.',
            'login_pin.4.required' => 'Please enter the fifth number.',
            'login_pin.4.numeric' => 'Please enter a number from 0-9.',
            'login_pin.5.required' => 'Please enter the sixth number.',
            'login_pin.5.numeric' => 'Please enter a number from 0-9.',
        ];
    }
}