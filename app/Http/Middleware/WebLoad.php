<?php

namespace Agspp\Http\Middleware;

use Closure;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Agspp\Models\ActivityLog\ActivityLog;
use Agspp\Models\Auth\Login;
use Agspp\Jobs\LogSiteActivity;
use Illuminate\Support\Facades\Auth;

class WebLoad
{
    use DispatchesJobs;

    protected $loginModel;

    public function __construct(Login $login)
    {
        $this->loginModel = $login;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /**
         * Set referrer.
         */
        $referrer = $request->server('HTTP_REFERER') ? $request->server('HTTP_REFERER') : 'direct';

        /**
         * Dispatch log api activity.
         */
        dispatch(new LogSiteActivity(
            ActivityLog::data($request, Auth::id(), $referrer, 200)
        ));

        /**
         * Check for remember cookie and login if set.
         */
        $this->loginModel->remember_cookie_check($request);

        /**
         * Return request.
         */
        return $next($request);
    }
}
