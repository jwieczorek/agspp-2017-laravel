<?php

namespace Agspp\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Agspp\Models\ApiKey\ApiKey as Handler;
use Agspp\Models\ActivityLog\ActivityLog;
use Agspp\Jobs\LogSiteActivity;
use Illuminate\Support\Facades\Auth;

class ApiV1
{
    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param  Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        /**
         * Generate new instance of ApiKey
         */
        $ApiKeyHandler = new Handler;

        /**
         * Validate api key request.
         */
        if(false === $ApiKeyHandler->validate($request)):

            /**
             * Dispatch log api activity.
             */
            dispatch(new LogSiteActivity(
                ActivityLog::data($request, null, 'api', 401)
            ));

            /**
             * Return 401 unauthorized response.
             */
            return Response::json(config('messages.auth.fail'), 401);
        endif;

        /**
         * Dispatch log api activity.
         */
        dispatch(new LogSiteActivity(
            ActivityLog::data($request, Auth::id(), 'api', 200)
        ));

        /**
         * Return request
         */
        return $next($request);
    }
}
