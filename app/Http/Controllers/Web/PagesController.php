<?php

namespace Agspp\Http\Controllers\Web;

use Agspp\Http\Controllers\Controller;

class PagesController extends Controller
{
    public function index($page='', $subPage='', $action='', $arg='')
    {
        echo $page, '<br>', $subPage, '<br>', $action, '<br>', $arg;
    }
}
