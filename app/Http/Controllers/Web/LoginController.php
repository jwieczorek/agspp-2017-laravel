<?php

namespace Agspp\Http\Controllers\Web;

use Agspp\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Agspp\Validation\Login as Validation;
use Agspp\Validation\LoginPin as ValidationPin;
use Agspp\Models\Auth\Login as Model;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        //$this->middleware('guest')->except('logout');
    }

    /**
     * GET | /login
     *
     * User Login For Entire Site
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        return view('forms.login');
    }

    /**
     * POST | /login
     *
     * Return login request pin.
     *
     * @param Model $login
     * @param Validation $validation
     * @param Request $request
     * @return mixed
     */
    public function login(Model $login, Validation $validation, Request $request)
    {
        /**
         * If validation errors return them and quit.
         */
        $validator = Validator::make($request->all(), $validation->rules(), $validation->messages());
        if($validator->fails()):
            return back()->withErrors($validator);
        endif;

        /**
         * Try to send login code.
         */
        if($login->try_send_code($request)):
            return redirect('login/'.session('login_pin'))->with('message', config('messages.auth.login.pinComing'));
        endif;

        /**
         * Otherwise redirect with message.
         */
        return back()->with('message', 'Sorry, something happened.');
    }

    /**
     * GET | /login/{pin}
     *
     * Verify login pin.
     *
     * @param Model $login
     * @param Request $request
     * @param $pin
     * @return mixed
     */
    public function pin(Model $login, Request $request, $pin)
    {
        /**
         * Validate pin.
         */
        if(!$login->verify_url_key($pin))
        {
            return redirect('login')->with('message', config('messages.auth.login.invalidKey'));
        }

        return view('forms.login-pin');
    }

    /**
     * POST | /login/{pin}
     *
     * Verify pin number.
     *
     * @param Model $login
     * @param Request $request
     * @return string
     */
    public function verify(Model $login, ValidationPin $validation, Request $request, $pin)
    {
        /**
         * Validate posted values.
         */
        $validator = Validator::make($request->all(), $validation->rules(), $validation->messages());
        if($validator->fails()):
            return back()->withErrors($validator);
        endif;

        /**
         * Verify correct pin.
         */
        if(false === $login->verify_pin($request, $pin)):
            return back()->with('message', config('messages.auth.login.invalidPin'));
        endif;

        /**
         * Check if redirect session isset and redirect to redirect_url.
         */
        if($redirect = get_redirect_url()):

            return redirect($redirect);

        /**
         * No redirect session then redirect to config redirect.
         */
        else:

            return redirect(config('urls.redirect.afterLogin'));

        endif;
    }
}
