<?php

namespace Agspp\Http\Controllers\Web;

use Agspp\Http\Controllers\Controller;

class UserController extends Controller
{
    public function index()
    {
        return 'User controller...';
    }

    public function profile()
    {
        return 'User profile...';
    }

    public function create()
    {
        return 'User created...';
    }

    public function update()
    {
        return 'User updating...';
    }
}
