<?php

namespace Agspp\Http\Controllers\Web;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Agspp\Http\Controllers\Controller;
use Agspp\Models\Auth\Register as Model;
use Agspp\Validation\Register as Validation;

class AccountController extends Controller
{
    public function index()
    {
        return view('forms.register');
    }

    /**
     * POST | /register
     *
     * Register new user.
     *
     * @param Model $register
     * @param Validation $validation
     * @param Request $request
     * @return string
     */
    public function register(Model $register, Validation $validation,  Request $request)
    {
        /**
         * If validation errors return them and quit.
         */
        $validator = Validator::make($request->all(), $validation->rules(), $validation->messages());
        if($validator->fails()):
            return back()->withErrors($validator)->withInput();
        endif;

        /**
         * Create user.
         */
        if(!$register->create($request)):
            return back()->with('messages', 'Something happened.');
        endif;

        return redirect('register/activate');
    }

    public function activate()
    {
        return 'activate your account.';
    }
}
