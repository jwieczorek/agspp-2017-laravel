<?php

namespace Agspp\Http\Controllers\Web;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Agspp\Http\Controllers\Controller;

class LogoutController extends Controller
{
    /**
     * POST | /logout
     *
     * Logs user out.
     *
     * @param Request $request
     * @return redirection
     */
    function index(Request $request)
    {
        Auth::logout();
        $request->session()->forget('usr_uname');
        $request->session()->forget('usr_fname');
        $request->session()->forget('usr_lname');
        $request->session()->forget('usr_email');
        $request->session()->forget('usr_uname');
        $request->session()->forget(config('site.cookies.remember'));
        return redirect(config('urls.redirect.afterLogout'))->withCookie('AgsppAuthentication', '', -90000);
    }
}
