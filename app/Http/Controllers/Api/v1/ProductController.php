<?php

namespace Agspp\Http\Controllers\Api\v1;
use Agspp\Http\Controllers\Api\ApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Agspp\Models\Shop\Product as ProductHandler;
use Agspp\Validation\CreateProduct;
use Agspp\Validation\UpdateProduct;

class ProductController extends ApiController
{
    /**
     * Get product.
     *
     * @param ProductHandler $handler
     * @param $product_id
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function get(ProductHandler $handler, $product_id)
    {
        $product = $handler->get($product_id);
        return (null !== $product) ? $product : $this->response(config('messages.product.notFound'), 404);
    }

    /**
     * Create Product.
     *
     * @param ProductHandler $handler
     * @param CreateProduct $validation
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(ProductHandler $handler, CreateProduct $validation, Request $request)
    {
        /**
         * If validation errors return them and quit.
         */
        $validator = Validator::make($request->all(), $validation->rules(), $validation->messages());
        if($validator->fails()):
            return $this->validationErrors($validator->messages());
        endif;

        /**
         * Try to save product and return successful response.
         */
        if($id = $handler->create($request)):
            return $this->response((
                config('messages.product.created')+['product_id' => $id]
            ), 201);
        endif;

        /**
         * Return fail response.
         */
        return $this->response(config('messages.product.createFail'), 500);
    }

    /**
     * Update Product.
     *
     * @param ProductHandler $handler
     * @param UpdateProduct $validation
     * @param Request $request
     * @param $product_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(ProductHandler $handler, UpdateProduct $validation, Request $request, $product_id)
    {
        /**
         * Validate product exists.
         */
        if(null === $product = $handler->get($product_id)):
            return $this->response(config('messages.product.notFound'), 404);
        endif;

        /**
         * If validation errors return them and quit.
         */
        $validator = Validator::make($request->all(), $validation->rules(), $validation->messages());
        if($validator->fails()):
            return $this->validationErrors($validator->messages());
        endif;

        /**
         * Try to updated product and return success message.
         */
        if($handler->update($request, $product_id)):
            return $this->response(config('messages.product.updated'), 200);
        endif;

        /**
         * Return fail response.
         */
        return $this->response(config('messages.product.updateFail'), 500);

    }
}
