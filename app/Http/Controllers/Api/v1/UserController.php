<?php

namespace Agspp\Http\Controllers\Api\v1;

use Agspp\Http\Controllers\Api\ApiController;
use Illuminate\Http\Request;
use Agspp\Handlers\User as Handler;

class UserController extends ApiController
{
    /**
     * Get user.
     *
     * @param Handler $handler
     * @param $user_id
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function get(Handler $handler, $user_id)
    {
        $user = $handler->get($user_id);

        if(null===$user):
            return $this->response(config('messages.user.notFound'), 404);
        endif;

        return $user;
    }

    public function create(Request $request)
    {

    }

    public function update(Request $request)
    {

    }

}
