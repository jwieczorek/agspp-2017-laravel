<?php

namespace Agspp\Http\Controllers\Api\v1;
use Agspp\Http\Controllers\Api\ApiController;
use Agspp\Handlers\SpotPrice\SpotPrice;

class SpotPriceController extends ApiController
{
    /**
     * Get spot prices.
     *
     * @param SpotPrice $handler
     * @return array
     */
    public function get(SpotPrice $handler)
    {
        return $handler->get();
    }
}
