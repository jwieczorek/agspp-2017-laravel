<?php

namespace Agspp\Http\Controllers\Api;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class ApiController extends BaseController
{
    use AuthorizesRequests, ValidatesRequests;

    /**
     * Return JSON response.
     *
     * @param $response
     * @param $status
     * @return \Illuminate\Http\JsonResponse
     */
    protected function response($response, $status=200)
    {
        return response()->json($response, $status);
    }

    /**
     * Return JSON error response.
     *
     * @param $errors
     * @return \Illuminate\Http\JsonResponse
     */
    protected function validationErrors($errors)
    {
        return response()->json(['ValidationErrors' => $errors], 400);
    }
}
