<?php

namespace Agspp\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \Illuminate\Foundation\Http\Middleware\ValidatePostSize::class,
        \Agspp\Http\Middleware\TrimStrings::class,
        \Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull::class,

    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [

        /**
         * Admin Middleware
         */
        'admin' => [
            \Agspp\Http\Middleware\Admin::class
        ],

        /**
         * Public Web Middleware
         */
        'web' => [
            \Agspp\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            \Illuminate\Session\Middleware\AuthenticateSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \Agspp\Http\Middleware\VerifyCsrfToken::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
            \Agspp\Http\Middleware\WebLoad::class,
        ],

        /**
         * Api V1 Middleware
         */
        'api1' => [
            \Agspp\Http\Middleware\ApiV1::class,
        ],

        /**
         * Api V2 Middleware
         */
        'api2' => [
            \Agspp\Http\Middleware\ApiV1::class,
        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \Illuminate\Auth\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'bindings' => \Illuminate\Routing\Middleware\SubstituteBindings::class,
        'can' => \Illuminate\Auth\Middleware\Authorize::class,
        'guest' => \Agspp\Http\Middleware\RedirectIfAuthenticated::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
        'admin' => \Agspp\Http\Middleware\Admin::class,
        'apiV1' => \Agspp\Http\Middleware\ApiV1::class,
        'apiV2' => \Agspp\Http\Middleware\ApiV2::class,
        'webload' => \Agspp\Http\Middleware\WebLoad::class
    ];
}
