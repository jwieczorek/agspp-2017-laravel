<?php

namespace Agspp\Entities;


class Log
{
    /**
     * Message.
     *
     * @var $message
     */
    public $message;

    /**
     * Message type.
     *
     * @var $type
     */
    public $type;

    /**
     * Log constructor.
     *
     * @param $message
     * @param $type
     */
    public function __construct($message, $type)
    {
        $this->message = $message;
        $this->type = $type;
    }
}