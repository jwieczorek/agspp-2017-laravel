<?php
/**
 * Created by PhpStorm.
 * User: joshuawieczorek
 * Date: 5/5/17
 * Time: 5:06 AM
 */

namespace Agspp\Entities;


class BaseEntity
{
    /**
     * Update entity attributes.
     *
     * @param array $request
     */
    public function update ($request=[])
    {
        foreach($request as $property => $value):
            if(property_exists($this, $property)):
                $this->$property = $value;
            endif;
        endforeach;
    }

    /**
     * Get request field value.
     *
     * @param array $request
     * @param string $name
     * @param string $default
     * @return mixed|string [field value or default]
     */
    protected function _field($request=[], $name='', $default='')
    {
        return isset($request[$name]) ? $request[$name] : $default;
    }

    /**
     * Generate meta array.
     *
     * @param array $meta_array
     * @return array
     */
    protected function _meta($meta_array=[])
    {
        $return_meta = [];
        foreach($meta_array as $key => $value):
            $return_meta[$key] = $value;
        endforeach;
        return $return_meta;
    }
}