<?php
/**
 * Created by PhpStorm.
 * User: joshuawieczorek
 * Date: 5/3/17
 * Time: 8:58 PM
 */

namespace Agspp\Entities;

use Illuminate\Support\Facades\Auth;

class Product extends BaseEntity
{
    public $id;
    public $creator_id;
    public $title;
    public $slug;
    public $guide;
    public $price;
    public $bulk_pricing;
    public $short_description;
    public $long_description;
    public $featured_image;
    public $type;
    public $access_role;
    public $status;
    public $created_at;
    public $updated_at;
    public $categories;
    public $tags;
    public $meta;

    public function __construct($request)
    {
        $this->id = $this->_field($request, 'id', null);
        $this->creator_id = Auth::user()->id;
        $this->title = $this->_field($request, 'title', '');
        $this->slug = str_slug($this->_field($request, 'slug', ''), '-');
        $this->guide = url('product/'.$this->slug);
        $this->price = $this->_field($request, 'price', []);
        $this->bulk_pricing = $this->_field($request, 'bulk_pricing', '');
        $this->short_description = $this->_field($request, 'short_description', '');
        $this->long_description = $this->_field($request, 'long_description', '');
        $this->featured_image = $this->_field($request, 'featured_image', '');
        $this->type = $this->_field($request, 'type', '');
        $this->status = $this->_field($request, 'status', '');
        $this->categories = $this->_field($request, 'categories', '');
        $this->tags = $this->_field($request, 'tags', '');
        $this->meta = $this->_meta($this->_field($request, 'meta', []));
    }
}