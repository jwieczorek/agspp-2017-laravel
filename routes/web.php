<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



/*
|--------------------------------------------------------------------------
| Home Routes
|--------------------------------------------------------------------------
*/
Route::get('/', 'Web\HomeController@index');

/*
|--------------------------------------------------------------------------
| Account
|--------------------------------------------------------------------------
*/
// Get requests
Route::get('login', 'Web\LoginController@index');
Route::get('login/{pin}', 'Web\LoginController@pin');
Route::get('register', 'Web\AccountController@index');
Route::get('register/activate', 'Web\AccountController@activate');
Route::get('account/login', 'Web\LoginController@index');
Route::get('account/register', 'Web\RegisterController@index');
Route::get('account/profile', 'Web\AccountController@profile');
Route::get('account', 'Web\AccountController@index');
Route::get('logout', 'Web\LogoutController@index');

// Post requests
Route::post('login', 'Web\LoginController@login');
Route::post('login/{pin}', 'Web\LoginController@verify');
Route::post('register', 'Web\AccountController@register');
Route::post('account/update', 'Web\RegisterController@update');

/*
|--------------------------------------------------------------------------
| Product Routes
|--------------------------------------------------------------------------
*/
Route::get('product/category/{id?}', 'Web\ProductController@category');
Route::get('product/tag/{id?}', 'Web\ProductController@tag');
Route::get('product/{id?}', 'Web\ProductController@product');
Route::get('products', 'Web\ProductController@index');

/*
|--------------------------------------------------------------------------
| Blog Routes
|--------------------------------------------------------------------------
*/
Route::get('blog/post/{id?}', 'Web\BlogController@post');
Route::get('blog/category/{id?}', 'Web\BlogController@category');
Route::get('blog/tag/{id?}', 'Web\BlogController@tag');
Route::get('blog', 'Web\BlogController@index');

/*
|--------------------------------------------------------------------------
| Page Routes
|--------------------------------------------------------------------------
*/
Route::get('{page}/{subPage?}/{action?}/{arg?}', 'Web\PagesController@index');