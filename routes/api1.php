<?php

/*
|--------------------------------------------------------------------------
| API V1 Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('tax', 'Api\v1\HomeController@index');

/*
|--------------------------------------------------------------------------
| Product Routes
|--------------------------------------------------------------------------
*/
/**
 * Get product.
 */
Route::get('product/{product_id}', 'Api\v1\ProductController@get')->where(
    'product_id', '[A-Za-z0-9\-]+'
);

/**
 * Create product.
 */
Route::post('product/create', 'Api\v1\ProductController@create');

/**
 * Update product.
 */
Route::put('product/update/{product_id}', 'Api\v1\ProductController@update')->where(
    'product_id', '[A-Za-z0-9\-]+'
);

/*
|--------------------------------------------------------------------------
| Users
|--------------------------------------------------------------------------
*/
/**
 * Get user.
 */
Route::get('user/{product_id}', 'Api\v1\UserController@get')->where(
    'user_id', '[A-Za-z0_]+'
);

/**
 * Create user.
 */
Route::post('user/create', 'Api\v1\UserController@create');

/**
 * Update user.
 */
Route::put('user/update/{user_id}', 'Api\v1\UserController@update')->where(
    'user_id', '[A-Za-z0_]+'
);

/*
|--------------------------------------------------------------------------
| Spot Prices
|--------------------------------------------------------------------------
*/
Route::get('spot-prices', 'Api\v1\SpotPriceController@get');