<?php

return [

    'redirect' => [
        'afterLogin' => '/',
        'afterLogout' => '/',
    ]

];