<?php
return [

    /**
     * Authentication messages.
     */
    'auth' => [
        'fail' => [
            'Status' => 'Fail',
            'message' => 'User is unauthorized to perform request!'
        ],

        'login' => [
            'pinComing' => 'Yea! Your PIN number is on it\'s way.',
            'invalidKey' => 'Invalid or expired pin, please get a new login pin.',
            'invalidPin' => 'Incorrect, please re-enter your pin, or get a new one.'
        ],
    ],

    /**
     * Product related messages.
     */
    'product' => [
        /**
         * Product created message.
         */
        'created' => [
            'Status' => 'Success',
            'Message' => 'Product created successfully!'
        ],

        /**
         * Product create fail message.
         */
        'createFail' => [
            'Status' => 'Fail',
            'Message' => 'Product was not created (save method did not save)!'
        ],

        /**
         * Product updated message.
         */
        'updated' => [
            'Status' => 'Success',
            'Message' => 'Product updated successfully!'
        ],

        /**
         * Product update fail message.
         */
        'updateFail' => [
            'Status' => 'Fail',
            'Message' => 'Product was not updated (save method did not save)!'
        ],

        /**
         * Product not found message.
         */
        'notFound' => [
            'Status' => 'Fail',
            'Message' => 'Product not found!'
        ]
    ],

    /**
     * User related messages.
     */
    'user' => [
        /**
         * User created message.
         */
        'created' => [
            'Status' => 'Success',
            'Message' => 'User created successfully!'
        ],

        /**
         * User create fail message.
         */
        'createFail' => [
            'Status' => 'Fail',
            'Message' => 'User was not created (save method did not save)!'
        ],

        /**
         * User updated message.
         */
        'updated' => [
            'Status' => 'Success',
            'Message' => 'User updated successfully!'
        ],

        /**
         * User update fail message.
         */
        'updateFail' => [
            'Status' => 'Fail',
            'Message' => 'User was not updated (save method did not save)!'
        ],

        /**
         * User not found message.
         */
        'notFound' => [
            'Status' => 'Fail',
            'Message' => 'User not found!'
        ]
    ],

    'sms' => [
        'loginCode' => "Here's Your AGSPP Login Token: %s"
    ],
];